import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
	kotlin("multiplatform")
	id("com.android.library")
}

android {
	namespace = "ic.graphics"
}

kotlin {

	android()

	val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget = when {
		System.getenv("SDK_NAME")?.startsWith("iphoneos") == true -> ::iosArm64
		else -> ::iosX64
	}

	iosTarget("ios") {
		binaries {
			framework {
				baseName = "pma"
			}
		}
	}

	sourceSets {
		val commonMain by getting {
			kotlin.srcDirs("src/common", "src/km")
			dependencies {
				implementation(project(":base"))
			}
		}
		val androidMain by getting {
			kotlin.srcDirs("src/jvm", "src/km-jvm", "src/android/java", "src/km-android/java")
			resources.srcDirs("src/android/res")
			dependencies {
				implementation(project(":base"))
				implementation("pl.droidsonroids.gif:android-gif-drawable:1.2.25")
			}
		}
		val iosMain by getting {
			kotlin.srcDirs("src/native", "src/km-native", "src/ios", "src/km-ios")
			dependencies {
				implementation(project(":base"))
			}
		}
	}

	targets.withType(KotlinNativeTarget::class.java) {
		binaries.all {
			binaryOptions["memoryModel"] = "experimental"
		}
	}

}

android {
	compileSdk = 33
	sourceSets["main"].manifest.srcFile("src/android/AndroidManifest.xml")
	sourceSets["main"].res.srcDirs("src/android/res")
	defaultConfig {
		minSdk = 21
		targetSdk = 33
	}
}